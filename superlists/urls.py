from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^$', 'A2.views.home_page', name='home'),
                       url(r'note_detail/(\d+)/$',
                           'A2.views.note_detail',
                           name='note_detail'),
                       url(r'note_edit/(\d+)/$',
                           'A2.views.note_edit',
                           name='note_edit'),
                       url(r'search_result/$',
                           'A2.views.search_result',
                           name='search_result'),
                       url(r'^accounts/login/$', 'A2.views.login_page',
                           name='login'),
                       url(r'^accounts/registration/$',
                           'A2.views.register_page',
                           name='register'),
                       url(r'^accounts/registration_complete/$',
                           'A2.views.register_complete_page',
                           name='register_complete'),
                       # password/change/
                       # password/reset/
                       # profile
                       # url(r'^blog/', include('blog.urls')),
                       # url(r'^admin/', include(admin.site.urls)),
                       )
