# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('A2', '0003_auto_20150420_0243'),
    ]

    operations = [
        migrations.RenameField(
            model_name='note',
            old_name='head',
            new_name='message',
        ),
        migrations.RenameField(
            model_name='note',
            old_name='text',
            new_name='title',
        ),
    ]
