import datetime

from django.db import models
from django.utils import timezone


class Note(models.Model):
    title = models.TextField(default='')
    url = models.TextField(default='')
    message = models.TextField(default='')
    pub_date = models.DateTimeField('date published')
