# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('A2', '0004_auto_20150420_0703'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='pub_date',
            field=models.DateTimeField(default=datetime.datetime(2015, 4, 22, 14, 14, 12, 723244, tzinfo=utc), verbose_name='date published'),
            preserve_default=False,
        ),
    ]
