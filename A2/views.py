from django.shortcuts import redirect, render
from A2.models import Note, datetime
from django.forms import ModelForm
from django.utils import timezone
from datetime import datetime
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
# registration
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

def home_page(request):
    if request.method == 'POST' and request.POST.get(
                                    'add_form', '') == 'Submit':
        title = request.POST['title_text']
        if (title == ''):
            title = 'Untitled'  # set Default
        url = request.POST['url_text']
        if (url == ''):
            url = 'None'  # set Default
        message = request.POST['note_text']
        pub_date = timezone.now()  # get timezone
        BKKTime = 7  # BKKtimezone + 7
        Note.objects.create(title=title, url=url,
                            message=message,
                            pub_date=pub_date.replace
                            (hour=(pub_date.hour+BKKTime) % 24))
        return redirect('/')

    # login back_send
    if (request.method == 'POST' and request.POST.get(
      'home_next', '') == 'Home_next'):
        return redirect('/')

    # login submit
    if(request.method == 'POST' and request.POST.get(
      'login_send', '') == 'send'):
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
        else:
            alarm = 'The username or password you entered is incorrect.'
            return render(request, 'registration/login.html', {'alarm': alarm})

    # log out
    if(request.method == 'POST' and request.POST.get(
      'submit_logout_page', '') == 'logout_next'):
        auth.logout(request)

    notes = Note.objects.all()
    for word in notes:  # split \n when user type [new line]
        word.message = word.message.split('\n')

    if request.method == 'POST' and request.POST.get(
                                    'sortbyname', '') == 'Sort by name':
        notes = Note.objects.all().order_by('title')
        if request.POST.get('sortselect', '') == 'descending':
            notes = Note.objects.all().order_by('-title')

    if request.method == 'POST' and request.POST.get(
                                    'sortbydate', '') == 'Sort by dates':
        notes = Note.objects.all().order_by('pub_date')
        if request.POST.get('sortselect', '') == 'descending':
            notes = Note.objects.all().order_by('-pub_date')

    return render(request, 'home.html', {'notes': notes})


def note_detail(request, note_id):
    each_note = Note.objects.get(id=note_id)  # get id
    each_note.message = each_note.message.split('\n')  # split \n
    # when user type [new line]
    if request.method == 'POST' and request.POST.get(
                                    'button_delete', '') == 'delete':
        deleted = request.POST['deleted']

        Note.objects.get(pk=deleted).delete()  # delete Note

        return redirect('/')

    return render(request, 'note_detail.html', {'each_note': each_note})


def note_edit(request, note_id):
    edit_note = Note.objects.get(id=note_id)  # get from id
    if request.method == 'POST' and request.POST.get(
                                    'edit_form', '') == 'Submit':
        Note.objects.get(id=note_id).delete()
        title = request.POST['title_text']
        if (title == ''):
            title = 'Untitled'  # set Default
        url = request.POST['url_text']
        if (url == ''):
            url = 'None'  # set Default
        message = request.POST['note_text']
        pub_date = timezone.now()  # get timezone
        BKKTime = 7  # BKKtimezone + 7
        edit_note = Note.objects.create(title=title, url=url,
                                        message=message,
                                        pub_date=pub_date.replace
                                        (hour=pub_date.hour+BKKTime))
        return redirect('/')

    return render(request, 'note_edit.html',
                  {'each_note': edit_note})


def search_result(request):
    if request.method == 'POST' and request.POST.get(
                                    'submit_search', '') == 'Search':
        search = request.POST['search']
    note_found = Note.objects.filter(title=search)  # get title
    for each_note_found in note_found:
        each_note_found.message = each_note_found.message.split('\n')
        # split \n when user type [new line]
    if note_found:
        return render(request, 'result.html',
                      {'note_found': note_found, 'search_word': search})
    else:
        return render(request, 'result404.html',
                      {'search_word': search})

# login ---
def login_page(request):
    if(request.method == 'POST' and request.POST.get(
      'submit_login_page', '') == 'login_next'):
        return redirect('/accounts/login/')
    return render(request, 'registration/login.html')


def register_page(request):
    if(request.method == 'POST' and request.POST.get(
      'submit_regis_page', '') == 'regis_next'):
        return redirect('/accounts/registration/')
    return render(request, 'registration/registration_form.html')


def register_complete_page(request):
    alarm = ''
    if(request.method == 'POST' and request.POST.get(
      'send_register', '') == 'send'):
        firstname = request.POST['firstname']
        lastname = request.POST['lastname']
        username = request.POST['username']
        password = request.POST['password1']
        password2 = request.POST['password2']
        email = request.POST['email']
        alarm = ''
        if (request.POST['password1'] == request.POST['password2'] and
           username != '' and request.POST['password1'] != ''):
            if User.objects.filter(username=username).count() == 0:
                new_user = User.objects.create_user(username, email, password)  # firstname, lastname, 
                new_user.is_staff = True
                new_user.save()
                return redirect('/accounts/registration_complete/')
            else:
                alarm = "Please provide the necessary information."
        else:
            alarm = "Please confirm your password again."

        return render(request,
                      'registration/registration_form.html', {'alarm': alarm})
    return render(request, 'registration/registration_complete.html')
