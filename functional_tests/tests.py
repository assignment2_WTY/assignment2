from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from A2.models import Note, datetime
from django.utils import timezone
import time

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_show_text')
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])

    def check_for_row_not_in_list_table(self, row_text):
        table = self.browser.find_element_by_id('id_show_text')
        rows = table.find_elements_by_tag_name('tr')
        self.assertNotIn(row_text, [row.text for row in rows])

    def check_date(self):
        pub_date = timezone.now()
        BKKTime = (pub_date.hour + 7)
        check_bkk = BKKTime
        BKKTime = BKKTime % 12  # อยู่ในช่วงเวลา 0-11
        timeis = time.localtime()

        checkmidnight = time.strftime('%p', timeis)
        midnight = 'p.m.'

        if(checkmidnight == 'AM'):
            if check_bkk >= 12:
                midnight = 'p.m.'
            else:
                midnight = 'a.m.'
        else:
            if check_bkk >= 12:
                midnight = 'a.m.'
            else:
                midnight = 'p.m.'

        realtime = time.strftime('%B %d, %Y, '+str(BKKTime)+':%M '+midnight,
                                 timeis)
        return realtime

    def test_can_start_a_list_and_retrieve_it_later(self):
        # เปิดหน้าเว็บขึ้นมา
        self.browser.get(self.live_server_url)

        # เช็คชื่อ title
        self.assertIn('Note Online', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        # เช็คชื่อหัวข้อ
        self.assertIn('Write Note Here', header_text)

        # เช็คกล่องใส่ข้อมูล title และเช็ค placeholder
        inputboxtitle = self.browser.find_element_by_id('id_title_text')
        self.assertEqual(
                inputboxtitle.get_attribute('placeholder'),
                'Write Title'
        )

        # เช็คกล่องใส่ข้อมูล url และเช็ค placeholder
        inputboxurl = self.browser.find_element_by_id('id_url_text')
        self.assertEqual(
                inputboxurl.get_attribute('placeholder'),
                'Write URL (e.g. http://www.google.com)'
        )

        # เช็คกล่องใส่ข้อมูล message และเช็ค placeholder
        inputboxmessage = self.browser.find_element_by_id('id_note_text')
        self.assertEqual(
                inputboxmessage.get_attribute('placeholder'),
                'Write Message'
        )


        # ให้แต่ละช่องใส่ข้อความ
        inputboxtitle.send_keys('Hello Google')
        inputboxurl.send_keys('http://www.google.com')
        inputboxmessage.send_keys('very good search engine')

        # ให้กดปุ่ม Submit
        button = self.browser.find_element_by_id('id_submit').click()

        # เรียกฟังก์ชั่นเช็คเวลา
        realtime = self.check_date()
        # เช็คว่ามีข้อความที่เราใส่ลงไป
        self.check_for_row_in_list_table('1. TITLE : Hello Google\nDate : '+realtime)

        # กำหนดปุ่มแต่ละปุ่ม
        inputboxtitle = self.browser.find_element_by_id('id_title_text')
        inputboxurl = self.browser.find_element_by_id('id_url_text')
        inputboxmessage = self.browser.find_element_by_id('id_note_text')
        # ให้แต่ละช่องใส่ข้อความ
        inputboxtitle.send_keys('Test Delete')
        inputboxurl.send_keys('http://www.google.co.th')
        inputboxmessage.send_keys('Delete')
        # ให้กดปุ่ม Submit
        button = self.browser.find_element_by_id('id_submit').click()

        # ให้กดปุ่ม more
        button = self.browser.find_element_by_id('view_note_2').click()
        # เช็คว่ามีข้อความที่เราใส่ลงไป
        self.check_for_row_in_list_table('TITLE : Test Delete')
        self.check_for_row_in_list_table('URL : http://www.google.co.th')
        self.check_for_row_in_list_table('Delete')
        # ให้กดปุ่ม delete
        button = self.browser.find_element_by_id('id_delete').click()

        # เช็คว่าไม่มีข้อความที่เราใส่ลงไป
        self.check_for_row_not_in_list_table('TITLE : Test Delete')
        self.check_for_row_not_in_list_table('URL : http://www.google.co.th')
        self.check_for_row_not_in_list_table('Delete')

        inputboxtitle = self.browser.find_element_by_id('id_title_text')
        inputboxmessage = self.browser.find_element_by_id('id_note_text')
        inputboxtitle.send_keys('Test Search')
        inputboxmessage.send_keys('Found')

        button = self.browser.find_element_by_id('id_submit').click()
        self.check_for_row_in_list_table('2. TITLE : Test Search\nDate : '+realtime)
        button = self.browser.find_element_by_id('view_note_3').click()

        # เช็คว่ามีข้อความที่เราใส่ลงไป
        self.check_for_row_in_list_table('TITLE : Test Search')
        self.check_for_row_in_list_table('URL : None')
        self.check_for_row_in_list_table('Found')

        self.fail('Finish the test!')

